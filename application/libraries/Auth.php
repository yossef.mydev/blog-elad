<?php

class Auth
{
    private $CI;

    public function __construct()
    {
        $this->CI = get_instance();
        $this->CI->load->library('session');
    }

    public function is_loggedin()
    {
        return $this->CI->session->has_userdata('user');
    }

    public function login($user)
    {
        unset($user['password']);
        
        $this->CI->session->set_userdata($user);
    }
}