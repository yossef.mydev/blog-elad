<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$data = [];
		
		$this->load->view('index_view', $data);
	}
	
	public function login()
	{
		$this->load->library('auth');
		
		if($this->auth->is_loggedin())
		redirect('blog');
		
		$this->load->helper('form');
		
		$data = [];
		
		$this->load->library('form_validation');
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('username', 'Username', 'callback_authorize_user['.$this->input->post("password").']');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('login_view');
		}
		else
		{
			redirect('blog');
		}
	}
	
	public function authorize_user($username, $password)
	{
		$this->load->model('user_model');
		
		if($user = $this->user_model->get_user($username))
		{
			if($password === $user['password'])
			{
				$this->auth->login($user);
				
				return TRUE;
			}
		}
		
		$this->form_validation->set_message('authorize_user', 'User name or password incorrect');
		
		return FALSE;
	}
}
