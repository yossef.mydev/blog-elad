<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Blog</title>
<link rel="stylesheet" href="<?= base_url() . 'assets/css/style.css'; ?>" />
</head>
<body>

<div id="container">
    <h1>Login</h1>
<?= form_open('blog/login');?>
<label for="username">User Name</label>
<?= form_input([
    'type'  => 'text',
    'name'  => 'username',
    'id'    => 'username'
]);?>
<label for="username">Password</label>
<?= form_input([
    'type'  => 'password',
    'name'  => 'password',
    'id'    => 'password'
]);?>
<button type="submit">Login</button>
<?= validation_errors(); ?>
</form>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</body>
</html>