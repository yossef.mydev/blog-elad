<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }
    
    public function get_user($email)
    {
        $this->db->select('password');
        $this->db->where('username', $email);
        $this->db->limit(1);
        
        $query = $this->db->get('users');
        
        if($query->num_rows() > 0)
            return $query->row_array();
        
        return FALSE;
    }
}